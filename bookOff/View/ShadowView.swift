//
//  ShadowView.swift
//  bookOff
//
//  Created by Blake Eram on 1/10/17.
//  Copyright © 2017 Blake Eram. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    
    override func awakeFromNib() {
        self.layer.shadowOpacity = 0.75
        self.layer.shadowRadius = 5
        self.layer.shadowColor = UIColor.black.cgColor
        super.awakeFromNib()
    }
    
}
