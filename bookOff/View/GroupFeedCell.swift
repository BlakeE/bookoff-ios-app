//
//  GroupFeedCell.swift
//  bookOff
//
//  Created by Blake Eram on 1/09/17.
//  Copyright © 2017 Blake Eram. All rights reserved.
//

import UIKit

//ui code for groupfeedcell
class GroupFeedCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    
    func configureCell(profileImage: UIImage, email: String, content: String) {
        self.profileImage.image = profileImage
        self.emailLbl.text = email
        self.contentLbl.text = content
    }
}
