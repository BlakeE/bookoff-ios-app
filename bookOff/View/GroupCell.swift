//
//  GroupCell.swift
//  bookOff
//
//  Created by Blake Eram on 1/09/17.
//  Copyright © 2017 Blake Eram. All rights reserved.
//

import UIKit

//groupcell ui code
class GroupCell: UITableViewCell {

    @IBOutlet weak var groupTitleLbl: UILabel!
    @IBOutlet weak var groupDescLbl: UILabel!
    @IBOutlet weak var memberCountLbl: UILabel!
    
    func configureCell(title: String, description: String, memberCount: Int) {
        self.groupTitleLbl.text = title
        self.groupDescLbl.text = description
        self.memberCountLbl.text = "\(memberCount) members."
    }
}
