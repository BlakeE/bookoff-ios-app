//
//  AuthVC.swift
//  bookOff
//
//  Created by Blake Eram on 1/10/17.
//  Copyright © 2017 Blake Eram. All rights reserved.
//

import UIKit
import Firebase

//For Authorization
class AuthVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            dismiss(animated: true, completion: nil)
        }
    }
    
    //brings up screen to log in 
    @IBAction func signInWithEmailBtnWasPressed(_ sender: Any) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        present(loginVC!, animated: true, completion: nil)
    }
    
    @IBAction func linkBtnWasPressed(_ sender: Any) {
        let linkVC = storyboard?.instantiateViewController(withIdentifier: "LinkVC")
        present(linkVC!, animated: true, completion: nil)
    }
    
}
