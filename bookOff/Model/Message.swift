//
//  Message.swift
//  bookOff
//
//  Created by Blake Eram on 1/12/17.
//  Copyright © 2017 Blake Eram. All rights reserved.
//

import Foundation

//message business logic, interacts with firebase
class Message {
    private var _content: String
    private var _senderId: String
    
    var content: String {
        return _content
    }
    
    var senderId: String {
        return _senderId
    }
    
    init(content: String, senderId: String) {
        self._content = content
        self._senderId = senderId
    }
}
